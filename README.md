# Project Euler
I have been noodling on solutions to **Project Euler** problems [https://projecteuler.net/about] using mostly **C++**. Haven't worked on this in the months since beginning my dissertation, but here is a list of the problems for which I've generated a solution:


- 1. Multiples of 3 and 5
- 2. Even Fibonacci numbers
- 3. Largest prime factor
- 4. Largest panlindrome product
- 5. Smallest Multiple
- 6. Sum square difference
- 7. 10001st prime
- 8. Largest product in a series
- 9. Special Pythagorean triplet
- 10. Summation of primes
- 11. Largest product in a grid
- 12. Highly divisible triangular number
- 13. Large sums
- 14. Longest Collatz sequence
- 16. Power digit sum
- 17. Number letter counts
- 18. Maximum path sum I 
- 20. Factorial digit sums
- 21. Amicable numbers
- 22. Names scores
- 27. Quadratic primes


The following are problems for which I've written a solution attempt but not worked out the bugs yet:

- 15. Lattice paths
- 23. Non-abundent sums
- 81. Path sum: two ways
- 142. Perfect square collection
- 164. Numbers for which no three consecutive digits have a sum greater than a given value
- 243. Resilience

